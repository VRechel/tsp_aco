\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Abk\IeC {\"u}rzungsverzeichnis}{ix}{chapter*.8}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Abbildungsverzeichnis}{x}{section*.9}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Tabellenverzeichnis}{xii}{section*.10}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Formelverzeichnis}{xiii}{section*.11}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Problemstellung}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Motivation}{1}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Ziel dieser Arbeit}{1}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Stand der Technik \& Forschung}{2}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Traveling Salesman Problem}{2}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Varianten des \ac {TSP}}{3}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}Reale Adaptionen}{3}{subsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Ant Colony Optimization}{3}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Arbeitsweise einer Ameisenkolonie}{4}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Addaption einer Ameisenkolonie in der Informatik}{5}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Numerische Datentypen}{5}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Begrenzungen von double}{6}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}M\IeC {\"o}glichkeiten durch BigDecimal}{6}{subsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}Probleme von BigDecimal}{6}{subsection.2.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Konzeptionierung}{8}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Software Engineering}{8}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Architektur}{9}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Persistenz}{10}{subsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Applikation}{10}{subsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}TSP}{11}{subsection.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.4}ACO}{11}{subsection.3.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.5}Arbeitsweise der Architektur}{12}{subsection.3.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Umsetzung der SOLID-Prinzipien}{18}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Single Responsibility}{18}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Open / Closed}{19}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}Liskov Substitution}{19}{subsection.3.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.4}Interface Segregation}{20}{subsection.3.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.5}Dependency Inversion}{20}{subsection.3.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Parameterbeschreibung}{21}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.1}Tau - $\tau $}{22}{subsection.3.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.2}Eta - $\eta $}{22}{subsection.3.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.3}Alpha - $\alpha $ und Beta - $\beta $}{22}{subsection.3.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}Theoretische Auswirkungen verschiedener Parameterverh\IeC {\"a}ltnisse}{22}{section.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.1}$\alpha $ hoch - $\beta $ niedrig}{23}{subsection.3.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.2}$\alpha $ mittel - $\beta $ mittel}{24}{subsection.3.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.3}$\alpha $ niedrig - $\beta $ hoch}{25}{subsection.3.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.6}Ausgew\IeC {\"a}hlte Algorithmen}{25}{section.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6.1}Ant - iteration()}{26}{subsection.3.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6.2}Colony - killAnt()}{26}{subsection.3.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6.3}Colony - updatePheromone()}{26}{subsection.3.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Implementierung}{28}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Beschreibung der Implementierung}{28}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Klassendiagramm}{28}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Persistenz}{29}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Applikation}{30}{subsection.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.4}TSP}{31}{subsection.4.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.5}ACO}{32}{subsection.4.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Beschreibung der verwendeten Datenstrukturen}{33}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}BigDecimal}{33}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}ArrayList}{34}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}HashMap vs zweidimensionales Array}{35}{subsection.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Beschreibung der Testabdeckung}{36}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Beweis der Funktionsf\IeC {\"a}higkeit}{37}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.1}numerisches Beispiel}{37}{subsection.4.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.2}a280 drilling problem}{38}{subsection.4.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}Performance-Analyse}{39}{section.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.1}CPU-Auslastung}{40}{subsection.4.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.2}Heap-Nutzung}{40}{subsection.4.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.3}Anzahl Threads pro Minute}{41}{subsection.4.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.4}Fazit zur Performance}{43}{subsection.4.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}\IeC {\"O}konomische Analyse}{44}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Grundlagen von COCOMO}{44}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Adaption auf die vorliegende Arbeit}{45}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Senstivit\IeC {\"a}tsanalyse}{47}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Erste Phase - Alpha zwischen 1 und 10}{48}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Zweite Phase - Alpha zwischen 0 und 4}{49}{section.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}Dritte Phase - Alpha zwischen 1 und 3}{50}{section.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.4}Vierte Phase - Alpha zwischen 2 und 2.5}{51}{section.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.5}Ergebnis der Sensitivit\IeC {\"a}tsanalyse}{52}{section.6.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Proof Of Concept}{54}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Zu berechnende Problemstellung}{54}{section.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}Laufzeitverhalten der Implementierung}{54}{section.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.3}Berechnete L\IeC {\"o}sung}{55}{section.7.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8}Optimierung der Softwarel\IeC {\"o}sung}{56}{chapter.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.1}Versuch der Umstellung der Pheromonmatrix auf BigDecimal}{56}{section.8.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.2}Optimierung der Datenbankzugriffe}{57}{section.8.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.3}Verwendete algorithmische Optimierungen}{57}{section.8.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.3.1}Doppelte Beachtung der derzeit besten Route}{57}{subsection.8.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.3.2}Regelm\IeC {\"a}\IeC {\ss }ige Verkleinerung aller Pheromonwerte}{58}{subsection.8.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.3.3}Mutation der Pheromonmatrix}{58}{subsection.8.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.3.4}Bezeichnung einzelner Ameisen als Elite}{58}{subsection.8.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {9}Fazit}{60}{chapter.9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9.1}Erreichte Ergebnisse}{60}{section.9.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9.2}Zuk\IeC {\"u}nftige Arbeiten}{60}{section.9.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Literaturverzeichnis}{61}{section*.57}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Anhang}{63}{section*.59}
