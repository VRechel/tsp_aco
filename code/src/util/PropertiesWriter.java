package util;

import java.io.*;
import java.util.Properties;

/**
 * @author Viktor Rechel
 * @package util
 * @date 2018-- 04
 */
public class PropertiesWriter {
    public void writeProperties(Properties properties){
        OutputStream output = null;

        try {
            output = new FileOutputStream("config.properties");
            properties.store(output, null);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
