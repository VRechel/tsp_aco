package aco;

import main.Configuration;
import tsp.City;
import util.MersenneTwisterFast;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.logging.Level;

/**
 * @author Viktor
 */
class Ant extends Thread {
    private final MersenneTwisterFast randomNumberGenerator;

    final int id;
    private final Colony colony;
    private CyclicBarrier barrier;
    private City currentCity;
    private ArrayList<City> route;
    private ArrayList<City> availableCities = new ArrayList<>();
    private boolean finished = false;
    private boolean elitist = false;

    /**
        Constructor for testing purposes. It is used to minimize the amount of variables to be initialized for testing.
     */
    Ant(int i, City start, Colony colony) {
        this.randomNumberGenerator = new MersenneTwisterFast((int) (Math.random() * 10));
        this.id = i;
        this.colony = colony;
        this.currentCity = start;
        this.route = new ArrayList<>();
        this.route.add(currentCity);
    }

    /**
        Constructor for productive use. All ants used by the program have to be initialized by using this constructor.

        @param i        An ID used to identify it by the application.
        @param start    The City from which the colony will start. All ants have to start at the same city.
        @param colony   The Colony the ant belongs to.
        @param barrier  A CyclicBarrier used to synchronize the threads (ants)
     */
    Ant(int i, City start, Colony colony, CyclicBarrier barrier) {
        this.randomNumberGenerator = new MersenneTwisterFast((int)(Math.random() * 10));
        this.id = i;
        this.colony = colony;
        this.currentCity = start;
        this.route = new ArrayList<>();
        this.route.add(currentCity);
        this.barrier = barrier;
    }

    /**
        The routine every ant has to run through. It will do a travel between every city while only visiting every city
        just once.
        For every city all reachable neighbours have to be accounted as a potential target.
     */
    public void run(){
        do {
            while(!finished) {
                double[] neighbours = Configuration.landscape.getSpecifiedNeighbours(this.getCurrentCity());

                ArrayList<City> neighboursList = new ArrayList<>();
                //Adding all neighbours of the current city to a temporary local list
                for (int x = 0; x < neighbours.length; x++) {
                    neighboursList.add(new City(x+1));
                }

                //Checks which of the neighbours can be reached (weren't visited before)
                for (int x = 0; x < neighboursList.size(); x++) {
                    if(this.route.contains(neighboursList.get(x))) {
                        neighboursList.remove(x);
                        x -= 1;
                    }
                }

                //Adds all reachable neighbours to a local list for easier comparison
                this.availableCities.clear();
                this.availableCities.addAll(neighboursList);

                //If all cities have been reached the ant has to travel back to the colony (the colony equals the first city)
                if(availableCities.size()==0) {
                    setFinished();
                    City start = this.route.get(0);
                    availableCities.add(start);
                    visitCity(start);
                } else {
                    boolean trip = false;
                    double idiocrazy = randomNumberGenerator.nextDouble();

                    //Idiocrazy(tm) filter
                    //A ant has a very low probability to just use a random city instead of checking the best path
                    //This is used to make sure that a single path will have too much weight
                    if(Configuration.idiocrazyEnabled){
                        if(idiocrazy < Configuration.idiocrazyFilter) {
                            int city = randomNumberGenerator.nextInt(availableCities.size());
                            visitCity(availableCities.get(city));
                            trip = true;
                        }
                    }

                    Map<City, BigDecimal> lambdas = calculateLambdas(this.availableCities);
                    Map<City, BigDecimal> probabilities = calculateProbabilities(this.availableCities, lambdas);

                    BigDecimal rand = BigDecimal.valueOf(randomNumberGenerator.nextDouble());

                    //If one city has a probability higher than the random number it will be targeted
                    if(!trip){
                        for (Map.Entry<City, BigDecimal> probability :
                                probabilities.entrySet()) {
                            if (probability.getValue().compareTo(rand) > 0) {
                                this.visitCity(probability.getKey());
                                trip = true;
                                break;
                            }
                        }
                    }
                    //If no city can be found which has a higher probability than rand than the probabilities will be added
                    // up until the sum is higher than rand.
                    //The city at which sum gets bigger than rand will be targeted.
                    if (!trip) {
                        BigDecimal sum = BigDecimal.valueOf(0.);
                        for (Map.Entry<City, BigDecimal> probability :
                                probabilities.entrySet()) {
                            sum = sum.add(probability.getValue());
                            if (sum.compareTo(rand) > 0) {
                                this.visitCity(probability.getKey());
                                trip = true;
                                break;
                            }
                        }
                    }
                    //This should never be achievable
                    //If the sum (which should be 1 after the adding up) is still smaller than the random number (smaller
                    // than 1) than the ant has to be killed.
                    if (!trip){
                        this.colony.killAnt(this);
                        return;
                    }
                }
            }
            //After the ants have arrived at the start again they have to wait for the others
            //When all ants finished they will update the colony
            try {
                this.printRoute();
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException ex) {
                System.out.println("Ant " + this.id + " was interrupted or has encountered a problem with the barrier!");
                return;
            } catch (NullPointerException ex) {
                System.out.println("Barrier is not existing! If this was not a test, check barrier initialization in Colony!");
                return;
            }
        }while(!Configuration.test);
    }

    /**
        An ant updates the colony by updating the pheromones and checking if its own route is better than the one from
        the colony.
    */
    void updateColony() {
        this.updatePheromones();
        this.colony.updateRoute(this.route);
    }

    /**
        The pheromones are updated by adding eta (1/distance) to every path taken by the ant
     */
    void updatePheromones() {
        for(int x = 0; x < route.size()-1; x++){
            BigDecimal distanceValue;
            if(this.colony.isPartOfBestRoute(this.route.get(x), this.route.get(x+1))){
                BigDecimal distance = BigDecimal.valueOf(Configuration.landscape.getDistance(route.get(x),route.get(x+1)));
                if(distance.compareTo(BigDecimal.ZERO) == 0)
                    distance = BigDecimal.valueOf(Double.MIN_VALUE);

                distanceValue = BigDecimal.valueOf(Configuration.pheromoneConstant)
                        .divide(distance, BigDecimal.ROUND_CEILING)
                        .add(BigDecimal.valueOf(Configuration.elitistFactor).divide(BigDecimal.valueOf(this.colony.getDistance(this.colony.getBestRoute())), BigDecimal.ROUND_CEILING))
                        .multiply(BigDecimal.valueOf(Configuration.elitistFactor));
            }
            else{
                BigDecimal distance = BigDecimal.valueOf(Configuration.landscape.getDistance(route.get(x),route.get(x+1)));
                if(distance.compareTo(BigDecimal.ZERO)  == 0)
                    distance = BigDecimal.valueOf(Double.MIN_VALUE);

                distanceValue = BigDecimal.valueOf(Configuration.pheromoneConstant)
                        .divide(distance, BigDecimal.ROUND_CEILING);
            }
            this.colony.updatePheromones(route.get(x),route.get(x+1), distanceValue.doubleValue());
        }
    }

    /**
        An ant travels between two cities. If the ant has the given city not as a reachable target she won't leave

        @param  b   The city the ant has to go to
     */
    void visitCity(City b){
        if(!availableCities.contains(b))
            return;
        this.currentCity = b;
        this.route.add(b);
    }

    /**
        For every reachable city the probability has to be calculated which will be used to decide the target

        @param  cities                  A list of the cities which can be reached
        @param  lambdas                 The list of lambdas which has to generated before calculating the probabilities
        @return Map<City, BigDecimal>   The list of probabilities
     */
    Map<City, BigDecimal> calculateProbabilities(ArrayList<City> cities, Map<City, BigDecimal> lambdas) {
        Map<City, BigDecimal> probabilities = new HashMap<>();
        BigDecimal sum = BigDecimal.ZERO;
        for (Map.Entry<City, BigDecimal> lambda: lambdas.entrySet()) {
            sum = sum.add(lambda.getValue());
        }
        for (City city: cities) {
            BigDecimal propability = calculateProbability(lambdas.get(city), sum);
            probabilities.put(city, propability);
        }
        return probabilities;
    }

    /**
        A probability to travel to a singe city is defined through the lambda and the sum of all lambdas

        @param  lambda  The lambda for a single city
        @param  sum  The sum of the lambdas of all reachable cities
        @return BigDecimal  The probability
     */
    BigDecimal calculateProbability(BigDecimal lambda, BigDecimal sum) {
        return lambda.divide(sum, BigDecimal.ROUND_CEILING);
    }

    /**
        From the list of reachable cities a HashMap of the cities and their lambda has to be created

        @param  cities       The list of all reachable cities
        @return Map<City, BigDecimal>   A mapping between the cities and the corresponding lambda
     */
    Map<City, BigDecimal> calculateLambdas(ArrayList<City> cities) {
        Map<City, BigDecimal> lambdas = new HashMap<>();
        for (City city:
             cities) {
            BigDecimal lambda = calculateLambda(
                    Configuration.landscape.getDistance(this.currentCity, city)
                    ,this.getColony().getPheromone(this.currentCity, city));
            if(lambda.compareTo(BigDecimal.ZERO) == 0)
                lambda = BigDecimal.valueOf(Double.MIN_VALUE);
            lambdas.put(city, lambda);
        }
        return lambdas;
    }

    /**
        A single lambda for a single city is calculated by multiplying tau (pheromone level of the path) and eta
        (inverse of the distance). Eta and tau are weighted with alpha and beta (from the colony=

        @param distance       The distance between the current city and the target
        @param pheromone       The pheromone level between both cities
        @return BigDecimal  Lambda
     */
    BigDecimal calculateLambda(double distance, double pheromone) {
        if(distance==0)
            return BigDecimal.ONE;

        BigDecimal tau = BigDecimal.valueOf(pheromone);
        tau = tau.pow((int) colony.getAlpha());
        if(colony.getAlpha() - (int) colony.getAlpha() > 0)
            tau = tau.add(BigDecimal.valueOf(Math.pow(tau.doubleValue(), ( colony.getAlpha() - (int) colony.getAlpha() ))));

        BigDecimal eta = BigDecimal.valueOf(1./distance);
        eta = eta.pow((int) colony.getBeta());
        if(colony.getBeta() - (int) colony.getBeta() > 0)
            eta = eta.add(BigDecimal.valueOf(Math.pow(eta.doubleValue(), ( colony.getBeta() - (int) colony.getBeta() ))));

        return eta.multiply(tau);
    }

    /**
        Every ant will log their route to the central log file.
     */
    private void printRoute() {
        StringBuilder sb = new StringBuilder();
        sb.append("Ant ").append(this.id).append(" Route: ");
        for (City city:
                this.route) {
            sb.append(city).append(",");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));

        sb.append(" Distance: ").append(this.colony.getDistance(this.route));
        Configuration.logger.log(Level.CONFIG, sb.toString());
    }

    Colony getColony() {
        return colony;
    }

    void setRoute(ArrayList<City> route) {
        this.route = route;
    }

    void setAvailableCities(ArrayList<City> availableCities) {
        this.availableCities = availableCities;
    }

    private void setFinished() {
        this.finished = true;
    }

    City getCurrentCity() {return currentCity;}

    ArrayList<City> getRoute() {
        return route;
    }

    void setCurrentCity(City currentCity) {
        this.currentCity = currentCity;
    }

    void setElitist(boolean elitist) {
        this.elitist = elitist;
    }

    void setFinished(boolean finished) {
        this.finished = finished;
    }

    boolean getElitist() {
        return this.elitist;
    }
}