package aco;

import main.Configuration;
import tsp.City;

import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;

public class Colony {
    //Parameters
    private final double alpha;
    private final double beta;
    private final double rho;
    private final City start = Configuration.landscape.getStartingCity();

    //Colony variables
    private boolean started;

    private boolean finished;
    private int currentGeneration = 1;
    private final ArrayList<Ant> ants = new ArrayList<>();

    //Calculation variables
    private static double[][] pheromones;
    private static double[] pheromoneRowConcentrations;

    private ArrayList<City> bestRoute = null;
    private ArrayList<City> currentBestRoute = null;
    private ArrayList<ArrayList<City>> generationRoutes = new ArrayList<>();
    private ArrayList<Double> generations = new ArrayList<>();
    private int bestRouteGeneration;
    private int stagnationCount = 0;
    private boolean newRoute = false;


    public Colony() {
        this.alpha = Configuration.alpha;
        this.beta = Configuration.beta;
        this.rho = Configuration.rho;
        initAnts();

        Configuration.getLogger().info(printConfig());
    }

    public Colony(double alpha, double beta) {
        this.alpha = alpha;
        this.beta = beta;
        this.rho = Configuration.rho;
        initAnts();

        Configuration.getLogger().info(printConfig());
    }

    public Colony(double alpha, double beta, double rho) {
        this.alpha = alpha;
        this.beta = beta;
        this.rho = rho;
        initAnts();

        Configuration.getLogger().info(printConfig());
    }

    /**
     * If the colony has not already been started all threads (ants) will be started
     */
    public void start() {
        Configuration.getLogger().info("Starting all ants!");
        if (!started) {
            started = true;
            for (Ant a :
                    ants) {
                a.start();
            }
        } else {
            Configuration.getLogger().severe("Colony already started!");
        }
    }

    /**
     * If a end condition (for example solution quality or time) are defined the colony has to stop at a point in time.
     */
    private void stop() {
        Configuration.getLogger().info("Stopping all ants!");
        if (started) {
            started = false;
            for (Ant a :
                    ants) {
                a.interrupt();
            }
        } else {
            Configuration.getLogger().severe("Colony not started!");
        }
        this.finished = true;
    }

    /**
     * If a generation finished an the colony is already updated it will generate a new generation of ants.
     * The debug parameter is only for testing purposes and will prevent any automatic start of the ants.
     */
    private void newGeneration() {
        generations.add(getDistance(currentBestRoute));
        /*
            The mutation of the pheromone matrix can be used to keep the values low and force the ants to use different
            ways.
         */
        if (Configuration.mutationEnabled)
            mutatePheromones();

        double currentDistance = getDistance(currentBestRoute);
        double bestDistance = getDistance(bestRoute);
        if(Configuration.stagnationResetEnabled && !this.newRoute) {
            stagnationCount = 1;
            int distance = generations.get(generations.size() -1).intValue();
            for (int x = 0; x < generations.size() && x < Configuration.maxStagnationCount - 1; x++) {
                if(generations.get(generations.size() - (x+1)).intValue() == distance){
                    stagnationCount++;
                }
            }
            if (stagnationCount != 0 && stagnationCount % ( Configuration.maxStagnationCount ) == 0) {
                initPheromone();
                stagnationCount = 0;
                Configuration.logger.info("Pheromones reseted!");
            }
        } else if (this.newRoute) {
            if ((int) currentDistance == (int) bestDistance)
                stagnationCount++;
            else
                stagnationCount = 0;
        }

        newRoute = false;
        generationRoutes = new ArrayList<>();
        currentBestRoute = null;
        this.currentGeneration++;
        if (!Configuration.test && (Configuration.maxGeneration >= currentGeneration))
            resetAnts();
        else
            stop();
    }

    /**
     * After every generation the ants will be reset.
     * The reset contains of deleting the current route and setting the home city as the current city.
     * If the ant had the best route in the last run it will be recognized as an elite ant which routes will be counted
     * additionally. An ant can loose its elitist status if its routes are worse than the best route.
     */
    void resetAnts() {
        for (Ant a :
                ants) {
            //if (a.getRoute() == bestRoute)
            //    a.setElitist(true);
            //if (a.getElitist()) {
            //    if (getDistance(a.getRoute()) > getDistance(bestRoute) * Configuration.elitistLimit)
            //        a.setElitist(false);
            //}

            a.setCurrentCity(start);
            a.setRoute(new ArrayList<>());
            a.getRoute().add(start);
            a.setFinished(false);
        }
    }

    /**
     * The colony will create a number of ants according to the value of the configuration parameter
     * Every ant will be synchronized with the same CyclicBarrier.
     */
    void initAnts() {
        CyclicBarrier barrier = new CyclicBarrier(Configuration.numberAnts, this::notifyColony);
        for (int i = 1; i < Configuration.numberAnts + 1; i++)
            ants.add(new Ant(i, start, this, barrier));
    }

    /**
     * The colony will initialize the 2D matrix for the pheromone values according to the neighbourhood matrix from
     * the landscape.
     */
    public void initPheromone() {
        int size = Configuration.landscape.getNeighboursSize();
        pheromones = new double[size][size];

        if (Configuration.useDecreaseStartPheromones) {
            for (int x = 0; x < size; x++) {
                for (int y = 0; y < size; y++) {
                    pheromones[x][y] = 1 / size;
                }
            }
        } else {
            for (int x = 0; x < size; x++) {
                for (int y = 0; y < size; y++) {
                    pheromones[x][y] = 0.0005;
                }
            }
        }
    }

    /**
     * If many generations calculate the exakt same result they are probably stuck in a local optimum.
     * If that happens the pheromone matrix will be reseted to a state in which it is normalized. Additonally the
     * current best route of all generations will be weighted one additional time so that the ants don't have to start
     * all over again.
     */
    private void resetPheromone() {
        int size = Configuration.landscape.getNeighboursSize();
        pheromones = new double[size][size];

        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                pheromones[x][y] = 1;
            }
        }
    }

    /**
     * The method which will be called by every ant caught by the barrier.
     * The colony will print the best route to the console and then generate a new generation of ants.
     */
    private void notifyColony() {
        Configuration.getLogger().info("\r\n Generation " + currentGeneration + " finished!");

        /*
            With regularly decreasing the pheromones levels it could be possible to keep a level where the ants keep
            trying new routes.
         */
        decreasePheromones();

        //After all ants have finished they have to distribute their new pheromones
        for (Ant a :
                ants) {
            a.updateColony();
        }

        double generationBestDistance = Double.MAX_VALUE;
        ArrayList<City> generationBestRoute = new ArrayList<>();
        for (ArrayList<City> route :
                generationRoutes) {
            double distance = getDistance(route);
            if (distance < generationBestDistance) {
                generationBestDistance = distance;
                generationBestRoute = route;
            }
        }
        Configuration.getCSVLogger(alpha, beta).info(String.valueOf(generationBestDistance) + ",");

        /*
            After the ants have finished the best distance of the current generation will be displayed and logged.
         */
        String currentDistance = printDistance(generationBestRoute);
        currentDistance = "Current best distance:\r\n" + currentDistance;
        Configuration.getLogger().info(currentDistance);
        /*
            The corresponding route to the best current distance will be logged as well.
         */
        String currentRoute = printRoute(generationBestRoute);
        currentRoute = "Current best route:\r\n" + currentRoute;
        Configuration.getLogger().fine(currentRoute);
        /*
            The best distance of all generations will be printed after the information of the current generation.
         */
        String colonyBestDistance = printDistance(bestRoute);
        colonyBestDistance = "Best distance from generation " + bestRouteGeneration + ":\r\n" + colonyBestDistance;
        Configuration.getLogger().info(colonyBestDistance);
        /*
            At last the best route of all generations will be printed - but not always logged (check logging level).
         */
        String colonyBestRoute = printRoute(bestRoute);
        colonyBestRoute = "Best route from generation " + bestRouteGeneration + ":\r\n" + colonyBestRoute;
        Configuration.getLogger().fine(colonyBestRoute);

        /*
            Every 100th generation the pheromon values of the colony will be printed.
         */
        if ((currentGeneration % 1000) == 0) {
            String pheromones = printPheromones();
            Configuration.getLogger().info(pheromones);
        }

        //After the distribution of the new pheromone values is finished the ants have to start over
        newGeneration();
    }

    /**
     * For every traveled path within the generation the value has to be updated.
     *
     * @param a      The source city
     * @param b      The target city
     * @param plevel The pheromone value which will be added to the current value in the pheromone matrix
     */
    synchronized void updatePheromones(City a, City b, double plevel) {
        double newValue = pheromones[a.getId()][b.getId()] + (plevel);
        pheromones[a.getId()][b.getId()] = newValue;
    }

    /**
     * The application will decrease the pheromone values on a regular basis. It could be done every generation or after
     * a count is reached. Check Configuration for further details.
     */
    void decreasePheromones() {
        if (Configuration.decreaseCountEnabled) {
            if (currentGeneration % Configuration.decreaseCount == 0) {
                decrease();
            }
        } else {
            decrease();
        }
    }

    /**
     * he actual decreasing goes over the pheromone matrix and decreases every value if possible. If the value would be
     * 0 it will be set to the minimal value of double.
     */
    private void decrease() {
        for (int x = 0; x < pheromones.length; x++) {
            for (int y = 0; y < pheromones.length; y++) {
                double temp = pheromones[x][y] * (1 - this.rho);
                if (temp == 0)
                    pheromones[x][y] = Double.MIN_VALUE;
                else
                    pheromones[x][y] = temp;
            }
        }
    }

    /**
     * The mutation of the pheromone matrix is used to keep it from being stuck in a local maximum/minimum.
     */
    void mutatePheromones() {
        pheromoneRowConcentrations = new double[pheromones.length];
        getPheromoneMatrixConcentration();
        for (int x = 0; x < pheromones.length; x++) {
            if (pheromoneRowConcentrations[x] > Configuration.maxPheromonRowConcentration) {
                if (Configuration.randomNumberGenerator.nextDouble() < Configuration.pheromoneMutationRatio)
                    mutateRow(x);
            }
        }

        double pheromoneMatrixConcentration = getPheromoneMatrixConcentration();
        if (pheromoneMatrixConcentration > Configuration.maxPheromonMatrixConcentration) {
            if (Configuration.randomNumberGenerator.nextDouble() < Configuration.pheromoneMutationRatio)
                mutateMatrix();
        }

    }

    /**
     * For determing which mutation steps have to be taken it is needed to compute the concentration of the pheromones.
     *
     * @param index The index of the row of the pheromon matrix
     */
    private void getPheromoneColumnConcentration(int index) {
        double sum = 0, highestValue = 0;
        for (int x = 0; x < pheromones.length; x++) {
            if (pheromones[x][index] > highestValue)
                highestValue = pheromones[x][index];
            sum += pheromones[x][index];
        }
       /* for (double value:
             pheromones[index]) {
            if(value > highestValue)
                highestValue = value;
            sum += value;
        }*/
        pheromoneRowConcentrations[index] = highestValue / sum;
    }

    private double getPheromoneMatrixConcentration() {
        double smallestConcentration = Double.MAX_VALUE;
        for (int x = 0; x < pheromones.length - 1; x++) {
            getPheromoneColumnConcentration(x);
            if (pheromoneRowConcentrations[x] < smallestConcentration)
                smallestConcentration = pheromoneRowConcentrations[x];
        }
        return smallestConcentration;
    }

    private void mutateRow(int indexY) {
        Configuration.getLogger().config("Mutating row " + indexY);
        double indexX = 0, highestValue = 0;
        for (int x = 0; x < pheromones.length - 1; x++) {
            if (pheromones[indexY][x] > highestValue) {
                highestValue = pheromones[x][indexY];
                indexX = x;
            }
        }
        double newValue = highestValue * Configuration.randomNumberGenerator.nextInt(1);
        double scrap = newValue / pheromones.length;
        for (int x = 0; x < pheromones.length - 1; x++) {
            if (x == indexX) {
                pheromones[x][indexY] = newValue;
            } else {
                pheromones[x][indexY] += scrap;
            }
        }

    }

    private void mutateMatrix() {
        Configuration.getLogger().info("Mutating matrix!");
        for (int x = 0; x < pheromones.length - 1; x++) {
            for (int y = 0; y < pheromones.length - 1; y++) {
                double newValue = pheromones[x][y] * Configuration.randomNumberGenerator.nextInt(1);
                if (newValue == 0)
                    newValue = Double.MIN_VALUE;
                double difference = pheromones[x][y] - newValue;
                double scrap = difference / pheromones.length;

                for (int i = 0; i < pheromones.length - 1; i++) {
                    for (int j = 0; j < pheromones.length - 1; j++) {
                        pheromones[i][j] += scrap;
                    }
                }
                pheromones[x][y] = newValue;
            }
        }
    }

    /**
     * Every ant checks if their route is better than the current best solution. If it is the route will be saved as
     * the new best route.
     *
     * @param route The route of cities
     */
    synchronized void updateRoute(ArrayList<City> route) {
        generationRoutes.add(route);
        if (bestRoute == null){
            bestRoute = route;
            newRoute = true;
        }
        if(currentBestRoute == null)
            currentBestRoute = route;
        double current = getDistance(bestRoute);
        double new_ = getDistance(route);
        if(new_ < getDistance(currentBestRoute))
            currentBestRoute = route;

        /*
          The database connection is a bottleneck for the application.
          It should only be used if desperately necessary.
         */
        //The database needs a String to save it not an ArrayList
        StringBuilder sRoute = new StringBuilder();
        if (!Configuration.debug) {
            for (City c :
                    route) {
                sRoute.append(c.toString()).append(",");
            }
            sRoute.reverse().deleteCharAt(0).reverse();
            //The column is currently maxed at 255 chars
            //If the String is longer we cut it off
            if (sRoute.length() > 255)
                sRoute.setLength(255);
            Configuration.dbManager.updateTable("GENERATIONS", currentGeneration, sRoute.toString(), new_);
        }
        if (new_ < current) {
            bestRouteGeneration = currentGeneration;
            bestRoute = route;
            newRoute = true;
            if (!Configuration.debug)
                Configuration.dbManager.updateTable("HISTORY", currentGeneration, sRoute.toString(), new_);
            /*
                The following code could be used to weigh the new best route additionally
             */
            for (int x = 0; x < route.size() - 1; x++) {
                City a = route.get(x);
                City b = route.get(x + 1);
                double distance = Configuration.landscape.getDistance(a, b);
                if (distance == 0)
                    distance = Double.MIN_VALUE;
                updatePheromones(a, b, (1. / distance));
            }
        }
    }

    /**
     * This method is only called by an ant which has to suicide. It will stop itself and tell the colony to remove it.
     *
     * @param a The ant which has to be killed
     */
    void killAnt(Ant a) {
        ants.remove(a);
        Configuration.getLogger().severe("ERROR: Ant " + a.id + " was killed!");
    }

    /**
     * The distance of a route is the fitness factor of this implementation. This method will sum up every traveled path.
     *
     * @param route The route of which the whole distance will be calculated
     * @return double  The distance value of the given route
     */
    double getDistance(ArrayList<City> route) {
        double sum = 0;
        for (int i = 0; i < route.size() - 1; i++) {
            sum += Configuration.landscape.getDistance(route.get(i), route.get(i + 1));
        }
        return sum;
    }

    /**
     * To determine of a part of the route of an art is elitist the current best route has to be checked if it contains
     * the combination of the two cities.
     *
     * @param cityA The city from which the ant starts
     * @param cityB The city to which the ant went
     * @return True if the best route contains the combination
     */
    boolean isPartOfBestRoute(City cityA, City cityB) {
        if (this.bestRoute == null)
            return false;
        int indexA = this.bestRoute.indexOf(cityA);
        return this.bestRoute.indexOf(cityB) == indexA + 1;
    }

    /**
     * The current pheromone matrix can be printed by going through the matrix and printing every value with 2 decimals.
     */
    private String printPheromones() {
        StringBuilder sb = new StringBuilder();
        sb.append(" Pheromones:\r\n");
        for (double[] pheromone : pheromones) {
            for (int y = 0; y < pheromones.length; y++) {
                sb.append(Math.floor(pheromone[y] * 1e2) / 1e2);
                sb.append(" \t");
            }
            sb.append("\r\n");
        }
        return sb.toString();
    }

    /**
     * A given route can be printed by printing out the cities in order of visit.
     *
     * @param route The given route
     */
    private String printRoute(ArrayList<City> route) {
        StringBuilder sb = new StringBuilder();
        sb.append(" Route: ");
        for (City city : route) {
            sb.append(city).append(",");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        return sb.toString();
    }

    /**
     * For a given route it is possible to print the distance.
     *
     * @param route The given route
     */
    private String printDistance(ArrayList<City> route) {
        return " Distance: " + getDistance(route);
    }

    /**
     * The configuration of the parameters alpha and beta will be written as an output before a colony start its
     * calculation.
     *
     * @return String  The string which will be written to the output
     */
    private String printConfig() {
        return "Configuration for Colony:" + "\r\n" +
                "Alpha: " + alpha + "\r\n" +
                "Beta: " + beta + "\r\n" +
                "Rho: " + rho + "\r\n";
    }

    double[][] getPheromones() {
        return pheromones;
    }

    double getAlpha() {
        return alpha;
    }

    double getBeta() {
        return beta;
    }

    double getPheromone(City a, City b) {
        return pheromones[a.getId()][b.getId()];
    }

    ArrayList<Ant> getAnts() {
        return ants;
    }

    public boolean isFinished() {
        return finished;
    }

    public ArrayList<City> getBestRoute() {
        return bestRoute;
    }
}