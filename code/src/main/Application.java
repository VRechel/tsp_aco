package main;

import aco.Colony;
import aco.PheromoneInitializationException;
import parser.Parser;
import parser.TSPParser;
import parser.XMLParser;
import util.DBInitializationException;

import java.io.File;

/**
 * @author Viktor
 */
class Application {
    /**
        First of all the application will start the corresponding parser to the file specified in the Configuration.
        The application will then startup the database which will be used by the colony to save the achievements of each
        generation.
        After that the colony will be initialized. The colony will handle the rest.
     */
    public static void main(String args[]) throws PheromoneInitializationException {
        try {
            Configuration.instance.readConfig();
        } catch (ConfigurationInitilizationException e) {
            System.out.println("Error while initializing configuration!");
            e.printStackTrace();
        }

        initTSP();
        try {
            initDB();
        } catch (DBInitializationException e) {
            System.out.println("DB manager already initialized!");
            Configuration.dbManager.dropTable();
        }
        Configuration.initLogger();

        Configuration.initCSVLogger(Configuration.alpha, Configuration.beta);
        Colony colony = initColony();
        while(!colony.isFinished()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /*
            Every start of the application will initiate a new database which has to shutdown at the end.
         */
        Configuration.dbManager.shutdown();
    }

    /**
        The application will decide which parser is needed and initialize it. Then the file will be read and the problem
        will be created.
     */
    private static void initTSP() {
        Parser parser;
        String path = Configuration.instance.getFilePath();
        switch (path.substring(path.lastIndexOf("."))) {
            case ".xml":
                parser = new XMLParser();
                parser.parse(new File(path));
                break;
            case ".tsp":
                parser = new TSPParser();
                parser.parse(new File(path));
                break;
            default:
                Configuration.getLogger().severe("File format not supported by now! Please use XML or TSP!");
                break;
        }
    }

    /**
        The HSQLDB has to be initialized at the first time.
     */
    private static void initDB() throws DBInitializationException {
        try {
            Configuration.dbManager.startup();
        } catch (DBInitializationException e) {
            throw new DBInitializationException();
        }
        Configuration.dbManager.init();
    }

    /**
        A new colony will be created. To function the colony has to initialize its pheromone matrix.
    */
    private static Colony initColony() {
        Colony colony = new Colony();
        colony.initPheromone();
        colony.start();
        return colony;
    }

    /**
        A new colony will be created with defined values for alpha and beta. This will be used to determine the best
        combination of both values.
     */
    private static Colony initColony(double alpha, double beta, double rho) throws PheromoneInitializationException {
        Colony colony = new Colony(alpha, beta, rho);
        colony.initPheromone();
        colony.start();
        return colony;
    }
}