package main;

/**
 * @author Viktor Rechel
 * @package main
 * @date 2018-- 04
 */
class ConfigurationInitilizationException extends Exception {
    ConfigurationInitilizationException() {
            super("Configuration already initialized!");
        }
}
