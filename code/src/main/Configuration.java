package main;

import tsp.Landscape;
import util.HSQLDBManager;
import util.MersenneTwisterFast;
import util.PropertiesReader;
import util.PropertiesWriter;

import java.io.IOException;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.*;

/**
 * @author Viktor
 */
public enum Configuration {
    instance;

    boolean initialized = false;
    private static final UUID id = UUID.randomUUID();

    //Init
    public static final MersenneTwisterFast randomNumberGenerator = new MersenneTwisterFast(System.currentTimeMillis());
    public static final HSQLDBManager dbManager = HSQLDBManager.instance;
    public static Landscape landscape = new Landscape();
    public static Logger logger;
    private static Logger csvlogger;
    private static final Level loggingLevel = Level.INFO;
    private final PropertiesReader rProperties = new util.PropertiesReader();
    private final PropertiesWriter wProperties = new util.PropertiesWriter();

    //Parameters
    /*
        Parameters for testing purposes:
        test will deactivate the restart of the generations
        debug will deactivate the saving of the data to the database
     */
    public static boolean test = false;
    public static final boolean debug = true;

    //The decreased start will initialize the pheromone matrix with a value of 1 / (dimension of matrix)
    public static final boolean useDecreaseStartPheromones = false;
    //The idiocrazy filter will choose a random city to visit by a very small chance
    public static final boolean idiocrazyEnabled = true;
    //The decrease count will not decrease the pheromone values every generation but after the speified count has been
    //reached
    public static final boolean decreaseCountEnabled = false;
    //The mutation tries to smooth the pheromone matrix so that the extreme values of an established route will be
    //decreased and new routes will be available again
    public static final boolean mutationEnabled = false;
    //The stagnation reset is a tool which checks if the last generation all got the same result. If they did the
    //pheromone matrix will be reseted so that the ants have to establish new routes. The best route will not be reseted
    //so that it will still be used for the elitist system.
    public static final boolean stagnationResetEnabled = true;


    public static int maxGeneration = 20000;            //The application will run for the amount of generations specified
    public static final double alpha = 2.2;             //How much the pheromones will be weighted
    public static final double beta = 2;                //How much the distance will be weighted
    public static final double rho = 0.2;               //How fast the pheromones will decrease (1 = instant vanish)
    public static final double pheromoneConstant = 0.9; //How fast new pheromones will be added up (>1 equals additional pheromones)
    final String filePath = "tspProblems/a280.tsp";
    public static final int numberAnts = 15;
    public static final double idiocrazyFilter = 0.005;
    public static final int decreaseCount = 10;
    public static final double elitistFactor = 2;
    //public static final double elitistLimit = 1.1;
    public static final double pheromoneMutationRatio = 0.015;
    public static final double maxPheromonRowConcentration = 0.9;
    public static final double maxPheromonMatrixConcentration = 0.75;
    public static final int maxStagnationCount = 50;

    //DB parameters
    public final static int batchMaxCount = 1000;
    private final String fileSeparator = System.getProperty("file.separator");
    private final String userDirectory = System.getProperty("user.dir");
    private final String dataDirectory = userDirectory + fileSeparator + "data" + fileSeparator;
    @SuppressWarnings("unused")
    public final String dataFilePath = dataDirectory + "tsp_antColony.csv";
    @SuppressWarnings("unused")
    public final String dataRDirectory = userDirectory;
    public final String databaseFile = dataDirectory + "datastore.db";

    public static Logger getLogger() {
        if (logger == null) {
            initLogger();
        }
        return logger;
    }

    public String getFilePath() {
        return filePath;
    }

    public static void initLogger() {
        try {
            //[%1$tc] Date/Time
            //%4$s: Level
            System.setProperty("java.util.logging.SimpleFormatter.format", " %5$s %n");

            logger = Logger.getLogger("general");
            for (Handler handler :
                    logger.getHandlers()) {
                logger.removeHandler(handler);
            }

            FileHandler fh = new FileHandler("./logs/Log_" + id + ".txt", true);
            fh.setFormatter(new SimpleFormatter());
            fh.setLevel(loggingLevel);

            ConsoleHandler ch = new ConsoleHandler();
            ch.setFormatter(new SimpleFormatter());
            ch.setLevel(loggingLevel);

            logger.addHandler(fh);
            logger.addHandler(ch);
            logger.setLevel(loggingLevel);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Logger getCSVLogger(double alpha, double beta) {
        if (csvlogger == null) {
            initCSVLogger(alpha, beta);
        }
        return csvlogger;
    }


    public static void initCSVLogger(double alpha, double beta) {
        try {
            System.setProperty("java.util.logging.SimpleFormatter.format", " %5$s %,");

            csvlogger = Logger.getLogger("csv_analyse");
            for (Handler handler :
                    csvlogger.getHandlers()) {
                csvlogger.removeHandler(handler);
            }

            FileHandler fh = new FileHandler("./logs/CSVLog_Alpha" + alpha + "Beta" + beta + ".csv", true);
            fh.setFormatter(new SimpleFormatter());
            fh.setLevel(loggingLevel);

            csvlogger.addHandler(fh);
            csvlogger.setLevel(loggingLevel);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void readConfig() throws ConfigurationInitilizationException {
        if (initialized)
            throw new ConfigurationInitilizationException();
        else {
            Properties properties = rProperties.readProperties();
            //TODO Properties einlesen
            initialized = true;
        }
    }

    public void writeConfig() {
        Properties properties = new Properties();
        //TODO Properties speichern
        wProperties.writeProperties(properties);
    }
}