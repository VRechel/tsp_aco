package util;

import main.Configuration;
import org.junit.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Viktor
 */
public class HSQLDBManagerTest {

    @Test
    public void initTest() {
        Configuration.dbManager.dropTable();
        Configuration.dbManager.init();
        try {
            Assert.assertEquals(0, Configuration.dbManager.getTable("GENERATIONS").getRow());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void updateTest() {
        String sqlStringBuilder = "INSERT INTO GENERATIONS ( id , generation, route , distance ) "+
                "VALUES ( "+ 1 + "," + 1 + "," + "'0,1,0'" + "," + 2 + " ) ";
        Configuration.dbManager.update(sqlStringBuilder);

        ResultSet rs = Configuration.dbManager.getTable("GENERATIONS");
        try {
            rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
        try {
            Assert.assertEquals(1,rs.getRow());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void batchUpdateTest() {
        Configuration.dbManager.setTest(true);
        String sqlStringBuilder = "INSERT INTO GENERATIONS ( id , generation, route , distance ) "+
                "VALUES ( "+ 1 + "," + 1 + "," + "'0,1,0'" + "," + 2 + " ) ";
        Configuration.dbManager.batchUpdate(sqlStringBuilder);
        String sqlStringBuilder2 = "INSERT INTO GENERATIONS ( id , generation, route , distance ) "+
                "VALUES ( "+ 2 + "," + 2 + "," + "'0,2,0'" + "," + 4 + " ) ";
        Configuration.dbManager.batchUpdate(sqlStringBuilder2);

        ResultSet rs = Configuration.dbManager.getTable("GENERATIONS");
        try {
            rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
        try {
            Assert.assertEquals(1,rs.getRow());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }

        try {
            rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
        try {
            Assert.assertEquals(2,rs.getRow());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void selectTest() {
        String sqlSelect = "SELECT * FROM GENERATIONS";

        String sqlUpdate = "INSERT INTO GENERATIONS ( id , generation, route , distance ) "+
                "VALUES ( "+ 1 + "," + 1 + "," + "'0,1,0'" + "," + 2 + " ) ";
        Configuration.dbManager.update(sqlUpdate);


        ResultSet rs = Configuration.dbManager.select(sqlSelect);
        try {
            if (rs != null) {
                rs.next();
                Assert.assertEquals(1, rs.getInt("generation"));
                Assert.assertEquals("0,1,0", rs.getString("route"));
                Assert.assertEquals(2, rs.getInt("distance"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void getTableTest() {
        Configuration.dbManager.init();
        try {
            Assert.assertEquals(4, Configuration.dbManager.getTable("GENERATIONS").getMetaData().getColumnCount());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }

        Configuration.dbManager.updateTable("GENERATIONS", 1, "0,1,2,0", 10);
        ResultSet result = Configuration.dbManager.getTable("GENERATIONS");
        try {
            result.next();
            Assert.assertEquals(1, result.getRow());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }


    @Test
    public void dropTableTest() {
        Configuration.dbManager.dropTable();
        Configuration.dbManager.createTable();
        try {
            Assert.assertEquals(4, Configuration.dbManager.getTable("GENERATIONS").getMetaData().getColumnCount());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
        Configuration.dbManager.dropTable();
        try {
            Assert.assertEquals(0, Configuration.dbManager.getTable("GENERATIONS").getMetaData().getColumnCount());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (NullPointerException ex){
            Assert.assertTrue(true);
        }
    }

    @Test
    public void createTableTest() {
        Configuration.dbManager.dropTable();
        Configuration.dbManager.createTable();
        try {
            Assert.assertEquals(4, Configuration.dbManager.getTable("GENERATIONS").getMetaData().getColumnCount());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
        try {
            Assert.assertEquals(4, Configuration.dbManager.getTable("HISTORY").getMetaData().getColumnCount());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
        try {
            Assert.assertEquals(0, Configuration.dbManager.getTable("TEST").getMetaData().getColumnCount());
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        } catch (NullPointerException ex){
            Assert.assertTrue(true);
        }
    }

    @Test
    public void updateTableTest() {
        Configuration.dbManager.updateTable("GENERATIONS", 1, "0,1,0", 2);

        ResultSet rs = Configuration.dbManager.getTable("GENERATIONS");
        try {
            rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            Assert.assertEquals(1,rs.getRow());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @After
    public void reset(){
        Configuration.dbManager.init();
    }

    @BeforeClass
    public static void init(){
        try{
            Configuration.dbManager.startup();
            Configuration.dbManager.init();
        }
        catch(DBInitializationException e){
            System.out.println(e.getMessage());
        }
    }

    @AfterClass
    public static void shutdown(){
        Configuration.dbManager.shutdown();
    }
}