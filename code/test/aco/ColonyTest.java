package aco;

import main.Configuration;
import org.junit.*;
import parser.Parser;
import parser.TSPParser;
import tsp.City;
import tsp.Landscape;

import java.io.File;
import java.util.ArrayList;

/**
 * @author Viktor
 */
public class ColonyTest {
    @Test
    public void updatePheromoneTest(){
        final City a = new City(1);
        final City b = new City(2);
        Colony colony = new Colony();
        colony.initPheromone();
        colony.updatePheromones(a,b, 5);
        double expectedValue = 1. + 5.;
        Assert.assertEquals(expectedValue,colony.getPheromone(a, b));
//        colony.updatePheromones(a,b, BigDecimal.valueOf(5));
//        BigDecimal expectedValue = BigDecimal.valueOf(1).add(BigDecimal.valueOf(5));
//        Assert.assertEquals(0, expectedValue.compareTo(colony.getPheromone(a, b)));
    }

    @Test
    public void initPheromonesTest(){
        final City a = new City(1);
        final City b = new City(2);
        Colony colony = new Colony();
        Configuration.landscape.addNeighbour(a,b,2);
        colony.initPheromone();
        Assert.assertEquals(2,colony.getPheromones().length);
        Assert.assertEquals(1, colony.getPheromones()[a.getId()][b.getId()]);
//        Assert.assertEquals(0, BigDecimal.valueOf(1).compareTo(colony.getPheromones()[a.getId()][b.getId()]));
    }

    @Test
    public void resetAntsTest(){
        Colony colony = new Colony();
        City city = new City(2);
        if(colony.getAnts().size()==0)
            Assert.fail();
        for(int i = 0; i < colony.getAnts().size(); i++){
            colony.getAnts().get(i).setCurrentCity(city);
        }
        for(int i = 0; i < colony.getAnts().size(); i++){
            if(colony.getAnts().get(i).getCurrentCity()!=city)
                Assert.fail();
        }
        colony.resetAnts();
        for(int i = 0; i < colony.getAnts().size(); i++){
            if(colony.getAnts().get(i).getCurrentCity()==city)
                Assert.fail();
        }

        Assert.assertEquals(Configuration.numberAnts, colony.getAnts().size());
    }

    @Test
    public void initAntsTest(){
        Colony colony = new Colony();
        if(colony.getAnts().size()==0)
            Assert.fail();
        for(int i = 0; i < colony.getAnts().size(); i++){
            colony.getAnts().remove(i);
            i--;
        }
        if(colony.getAnts().size()!=0)
            Assert.fail();
        else
            colony.initAnts();
        Assert.assertEquals(Configuration.numberAnts, colony.getAnts().size());
    }

    @Test
    public void killAntTest(){
        Colony colony = new Colony();
        if(colony.getAnts().size() == 0)
            Assert.fail();
        Ant a = colony.getAnts().get(0);
        colony.killAnt(a);
        Assert.assertTrue(!colony.getAnts().contains(a));
    }

//    @Test
//    public void printPheromonesTest(){
//        final City a = new City(1);
//        final City b = new City(2);
//        Configuration.instance.landscape.addNeighbour(a,b,2);
//        Configuration.instance.landscape.addNeighbour(b,a,2);
//
//        Colony colony = new Colony();
//        try {
//            colony.initPheromone();
//        } catch (PheromoneInitializationException e) {
//            e.printStackTrace();
//        }
//    }

//    @Test
//    public void notifyColonyTest(){
//        final City a = new City(1);
//        final City b = new City(2);
//
//        Configuration.instance.landscape.addNeighbour(a,b,2);
//        Configuration.instance.landscape.addNeighbour(b,a,2);
//
//        Colony colony = new Colony();
//        ArrayList<City> temp = new ArrayList<>();
//        temp.add(a);
//        temp.add(b);
//        colony.updateRoute(temp);
//
//        colony.notifyColony();
//
//        Assert.assertTrue(!colony.started);
//        Assert.assertTrue(colony.getAnts().size()>0);
//        Assert.assertEquals(2, colony.currentGeneration);
//    }

    @Test
    public void mutatePheromonesTest(){
        Colony colony = new Colony();

        for (int x = 0; x < colony.getPheromones().length; x++) {
            for (int y = 0; y < colony.getPheromones().length; y++) {
                colony.getPheromones()[x][y] = 2.;
            }
        }

        colony.mutatePheromones();

        boolean noChange = true;
        for (int x = 0; x < colony.getPheromones().length; x++) {
            for (int y = 0; y < colony.getPheromones().length; y++) {
                if(colony.getPheromones()[x][y] == 1){
                    noChange = false;
                }
            }
        }

        if (noChange)
            colony.mutatePheromones();

        for (int x = 0; x < colony.getPheromones().length; x++) {
            for (int y = 0; y < colony.getPheromones().length; y++) {
                if(colony.getPheromones()[x][y] == 1){
                    noChange = false;
                }
            }
        }

        Assert.assertTrue(!noChange);
    }

    @Test
    public void decreasePheromonesTest(){
        Colony colony = new Colony();

        for (int x = 0; x < colony.getPheromones().length; x++) {
            for (int y = 0; y < colony.getPheromones().length; y++) {
                if(x == 2 && y == 1)
                    colony.getPheromones()[x][y] = 23.;
                else
                    colony.getPheromones()[x][y] = 2.;
            }
        }

        colony.decreasePheromones();

        for (int x = 0; x < colony.getPheromones().length; x++) {
            for (int y = 0; y < colony.getPheromones().length; y++) {
                if(x == 2 && y == 1)
                    Assert.assertEquals(3, colony.getPheromones()[x][y]);
                else
                    Assert.assertEquals(2, colony.getPheromones()[x][y]);
            }
        }
    }

    @Test
    public void distanceCheck(){
        Parser parser = new TSPParser();
        parser.parse(new File(Configuration.instance.getFilePath()));
        Colony colony = new Colony();
        int[] temp = {1, 2, 242, 243, 244, 241, 240, 239, 238, 237, 236, 235, 234, 233, 232, 231, 246, 245, 247, 250,
                251, 230, 229, 228, 227, 226, 225, 224, 223, 222, 221, 220, 219, 218, 217, 216, 215, 214, 213, 212, 211,
                210, 207, 206, 205, 204, 203, 202, 201, 198, 197, 196, 195, 194, 193, 192, 191, 190, 189, 188, 187, 186,
                185, 184, 183, 182, 181, 176, 180, 179, 150, 178, 177, 151, 152, 156, 153, 155, 154, 129, 130, 131, 20,
                21, 128, 127, 126, 125, 124, 123, 122, 121, 120, 119, 157, 158, 159, 160, 175, 161, 162, 163, 164, 165,
                166, 167, 168, 169, 170, 172, 171, 173, 174, 107, 106, 105, 104, 103, 102, 101, 100, 99, 98, 97, 96, 95,
                94, 93, 92, 91, 90, 89, 109, 108, 110, 111, 112, 88, 87, 113, 114, 115, 117, 116, 86, 85, 84, 83, 82, 81,
                80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70, 69, 68, 67, 66, 65, 64, 58, 57, 56, 55, 54, 53, 52, 51, 50,
                49, 48, 47, 46, 45, 44, 59, 63, 62, 118, 61, 60, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30,
                29, 28, 27, 26, 22, 25, 23, 24, 14, 15, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 277, 276, 275, 274, 273, 272,
                271, 16, 17, 18, 19, 132, 133, 134, 270, 269, 135, 136, 268, 267, 137, 138, 139, 149, 148, 147, 146, 145,
                199, 200, 144, 143, 142, 141, 140, 266, 265, 264, 263, 262, 261, 260, 259, 258, 257, 254, 253, 208, 209,
                252, 255, 256, 249, 248, 278, 279, 3, 280, 1};

        ArrayList<City> tempRoute = new ArrayList<>();
        for (Integer i:
                temp) {
            tempRoute.add(new City(i));
        }

        Assert.assertEquals(281,tempRoute.size());
        Assert.assertEquals(2586.76, colony.getDistance(tempRoute),0.01);
    }

    @Before
    @After
    public void resetLandscape(){
        Configuration.landscape = new Landscape();
    }

    @BeforeClass
    public static void enableTestMode(){
        Configuration.test = true;
    }

    @AfterClass
    public static void disableTestMode(){
        Configuration.test = false;
    }
}
